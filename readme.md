# Beispiele zum Artikel "Koroutinen in Kotlin: Einführung" im JavaMagazin

Unter *src/test/kotlin* findet sich eine Reihe von Testklassen, aus denen die freistehenden Beispiele für
Threads, Callbacks, RxJava und Koroutinen entnommen worden sind.

Unter *main/src/kotlin* finden sich die Datei *CollageFunktionen* in der
alle Koroutinen für die Collage enthalten sind. Zusätzlich noch die Datei *KtorWebserver*,
 die die vollständige Service-Implementierung enthält.