package de.e2.koroutine1

import com.jayway.jsonpath.JsonPath
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.awt.image.BufferedImage
import java.io.FileOutputStream
import java.io.InputStream
import javax.imageio.ImageIO
import javax.ws.rs.core.MediaType


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CollageMitThreads {


    @Test
    fun `01 Ein Bild laden mit Threads`() {

        fun loadOneImage(query: String): BufferedImage {
            val url = requestImageUrl(query)
            val image = requestImageData(url)
            return image
        }

        val image = loadOneImage("dogs")
        ImageIO.write(image, "png", FileOutputStream("dogs-1.png"))
        println("${image.width}x${image.height}")
    }

    @Test
    fun `02 Mehrere Bilder laden mit Threads`() {

        fun createCollage(query: String, count: Int): BufferedImage {
            val urls = requestImageUrls(query, count)
            val images = urls.map { requestImageData(it) }
            return combineImages(images)
        }

        val image = createCollage("dogs", 20)
        ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
        println("${image.width}x${image.height}")
    }


    fun requestImageUrl(query: String): String {
        val json = JerseyClient.qwant("q=$query&count=200")
            .request()
            .get(String::class.java)
        return JsonPath.read<List<String>>(json, "$..thumbnail").shuffled().firstOrNull()?.let { "https://$it" }
            ?: throw IllegalStateException("No image found")
    }

    fun requestImageUrls(query: String, count: Int = 20): List<String> {
        val json = JerseyClient.qwant("q=$query&count=$count")
            .request()
            .get(String::class.java)
        return JsonPath.read<List<String>>(json, "$..thumbnail").map { "https:$it" }
    }

    fun requestImageData(imageUrl: String): BufferedImage {
        val inputStream = JerseyClient.url(imageUrl)
            .request(MediaType.APPLICATION_OCTET_STREAM)
            .get(InputStream::class.java)
        return ImageIO.read(inputStream)
    }

    @AfterAll
    fun closeJerseyClient() {
        JerseyClient.close()
    }
}


