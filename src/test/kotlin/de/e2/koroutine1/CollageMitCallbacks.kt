package de.e2.koroutine1

import com.jayway.jsonpath.JsonPath
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.awt.image.BufferedImage
import java.io.FileOutputStream
import java.io.InputStream
import javax.imageio.ImageIO
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType

typealias OnSuccess<T> = (T) -> Unit
typealias OnFailure = (Throwable) -> Unit

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CollageMitCallbacks {


    @Test
    fun `01 Ein Bild laden mit Callbacks`() {

        fun loadOneImage(query: String, callback: (BufferedImage) -> Unit) {
            requestImageUrl(query) { url ->
                requestImageData(url) { image ->
                    callback(image)
                }
            }
        }

        loadOneImage("dogs") { image ->
            ImageIO.write(image, "png", FileOutputStream("dogs-1.png"))
            println("${image.width}x${image.height}")
        }

        Thread.sleep(5000)
    }

    @Test
    fun `02 Mehrere Bilder laden mit Callbacks`() {

        fun createCollage(query: String, count: Int, onSuccess: OnSuccess<BufferedImage>) {
            requestImageUrls(query, count) { urls ->
                fun loadImage(
                    urlIter: Iterator<String>,
                    retrievedImages: List<BufferedImage>
                ) {
                    if (urlIter.hasNext()) {
                        requestImageData(urlIter.next()) { image ->
                            loadImage(urlIter, retrievedImages + image)
                        }
                    } else {
                        onSuccess(combineImages(retrievedImages))
                    }
                }
                loadImage(urls.iterator(), listOf())
            }
        }

        createCollage("dogs", 20) { image ->
            ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
            println("${image.width}x${image.height}")
        }

        Thread.sleep(5000)
    }


    fun requestImageUrl(
        query: String, onFailure: OnFailure = {}, onSuccess: OnSuccess<String>
    ) {
        JerseyClient.qwant("q=$query&count=1")
            .request()
            .async()
            .get(object : InvocationCallback<String> {
                override fun completed(response: String) {
                    val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                    val url = urls.firstOrNull() ?: return failed(IllegalStateException("No image found"))
                    onSuccess(url)
                }

                override fun failed(throwable: Throwable) {
                    onFailure(throwable)
                }
            })
    }

    fun requestImageUrls(
        query: String,
        count: Int = 20,
        onFailure: OnFailure = {},
        onSuccess: OnSuccess<List<String>>
    ) {
        JerseyClient.qwant("q=$query&count=$count")
            .request()
            .async()
            .get(object : InvocationCallback<String> {
                override fun completed(response: String) {
                    val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                    onSuccess(urls)
                }

                override fun failed(throwable: Throwable) {
                    onFailure(throwable)
                }
            })
    }

    fun requestImageData(
        imageUrl: String, onFailure: OnFailure = {}, onSuccess: OnSuccess<BufferedImage>
    ) {
        JerseyClient.url(imageUrl)
            .request(MediaType.APPLICATION_OCTET_STREAM)
            .async()
            .get(object : InvocationCallback<InputStream> {
                override fun completed(response: InputStream) {
                    val image = ImageIO.read(response)
                    onSuccess(image)
                }

                override fun failed(throwable: Throwable) {
                    onFailure(throwable)
                }
            })
    }

    @AfterAll
    fun closeJerseyClient() {
        JerseyClient.close()
    }
}


