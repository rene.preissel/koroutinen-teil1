package p07_koroutinen

import de.e2.koroutine1.createCollage
import de.e2.koroutine1.createCollageAsyncAwait
import de.e2.koroutine1.loadOneImage
import javafx.embed.swing.SwingFXUtils
import javafx.scene.image.ImageView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.time.delay
import kotlinx.coroutines.withContext
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.FileOutputStream
import java.time.Duration
import java.util.concurrent.Executors.newFixedThreadPool
import javax.imageio.ImageIO
import kotlin.coroutines.CoroutineContext


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CollageMitKoroutinen {


    @Test
    fun `01 Ein Bild mit Koroutinen laden`() = runBlocking {
        val image = loadOneImage("dogs")
        ImageIO.write(image, "png", FileOutputStream("dogs-1.png"))
        println("${image.width}x${image.height}")
    }

    @Test
    fun `02 Collage mit Koroutinen laden`() = runBlocking {
        val image = createCollage("dogs", 20)
        ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
        println("${image.width}x${image.height}")
    }

    @Test
    fun `03 Collage mit eigenen Kontext`() = runBlocking {

        class ClientRequest : CoroutineScope {
            override val coroutineContext: CoroutineContext = Dispatchers.IO
        }

        val clientRequest = ClientRequest()

        val dispatcher = newFixedThreadPool(2).asCoroutineDispatcher()
        val job = clientRequest.launch(dispatcher) {
            val image = createCollage("dogs", 20)
            ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
            println("${image.width}x${image.height}")
        }


        job.join()
    }

    @Test
    fun `04 Kontext wechseln`() = runBlocking {

        class JavaFXPanel : CoroutineScope {
            override val coroutineContext: CoroutineContext = Dispatchers.IO
        }

        val javaFXPanel = JavaFXPanel()

        val job = javaFXPanel.launch(Dispatchers.Main) {
            val imageView = ImageView()

            val image = withContext(Dispatchers.IO) {
                createCollage("dogs", 20)
            }

            val imageFx = SwingFXUtils.toFXImage(image, null)
            imageView.setImage(imageFx)
        }
        job.join()

    }

    @Test
    fun `05 Async Await mit Koroutinen`() = runBlocking {

        class ClientRequest : CoroutineScope {
            override val coroutineContext: CoroutineContext = Dispatchers.IO
        }

        val clientRequest = ClientRequest()

        val job = clientRequest.launch {
            // this ist CoroutineScope
            val result1: Deferred<Int> = async {
                delay(Duration.ofSeconds(1))
                1
            }
            val result2: Deferred<Int> = async {
                delay(Duration.ofSeconds(1))
                2
            }
            println(result1.await() + result2.await())
        }

        job.join()
    }

    @Test
    fun `06 Collage asynchron laden`() = runBlocking {

        val image = createCollageAsyncAwait("dogs", 20)
        ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
        println("${image.width}x${image.height}")
    }

}


