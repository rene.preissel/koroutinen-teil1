package de.e2.koroutine1

import com.jayway.jsonpath.JsonPath
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.awt.image.BufferedImage
import java.io.FileOutputStream
import java.io.InputStream
import javax.imageio.ImageIO
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CollageMitRxJava {


    @Test
    fun `01 Ein Bild laden mit RxJava`() {

        fun loadOneImage(query: String): Single<BufferedImage> {
            return requestImageUrl(query).flatMap(::requestImageData)

        }

        val image = loadOneImage("dogs").blockingGet()
        ImageIO.write(image, "png", FileOutputStream("dogs-1.png"))
        println("${image.width}x${image.height}")

    }

    @Test
    fun `02 Mehrere Bilder laden mit RxJava`() {
        fun createCollage(query: String, count: Int): Single<BufferedImage> {
            return requestImageUrls(query, count)
                .flatMapObservable { urls ->
                    Observable.fromIterable(urls)
                        .concatMapSingle { requestImageData(it) }
                }.toList().map { combineImages(it) }
        }

        val image = createCollage("dogs", 20).blockingGet()
        ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
        println("${image.width}x${image.height}")
    }


    fun requestImageUrl(query: String) = Single.create<String> { emitter ->
        JerseyClient.qwant("q=$query&count=200")
            .request()
            .async()
            .get(object : InvocationCallback<String> {
                override fun completed(response: String) {
                    val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                    val url = urls.shuffled().firstOrNull() ?: return failed(IllegalStateException("No image found"))
                    emitter.onSuccess(url)
                }

                override fun failed(throwable: Throwable) {
                    emitter.onError(throwable)
                }
            })
    }

    fun requestImageUrls(query: String, count: Int) =
        Single.create<List<String>> { emitter ->
            JerseyClient.qwant("q=$query&count=$count")
                .request()
                .async()
                .get(object : InvocationCallback<String> {
                    override fun completed(response: String) {
                        val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                        emitter.onSuccess(urls)
                    }

                    override fun failed(throwable: Throwable) {
                        emitter.onError(throwable)
                    }
                })
        }

    fun requestImageData(imageUrl: String) = Single.create<BufferedImage> { emitter ->
        println("start requestImageData $imageUrl")
        JerseyClient.url(imageUrl)
            .request(MediaType.APPLICATION_OCTET_STREAM)
            .async()
            .get(object : InvocationCallback<InputStream> {
                override fun completed(response: InputStream) {
                    println("finish requestImageData $imageUrl")
                    val image = ImageIO.read(response)
                    emitter.onSuccess(image)
                }

                override fun failed(throwable: Throwable) {
                    emitter.onError(throwable)
                }
            })
    }

    @AfterAll
    fun closeJerseyClient() {
        JerseyClient.close()
    }
}


