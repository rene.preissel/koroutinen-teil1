package de.e2.koroutine1

import org.glassfish.jersey.client.ClientConfig
import java.io.Closeable
import javax.ws.rs.client.ClientBuilder


object JerseyClient : Closeable {
    private val client = ClientBuilder.newClient(ClientConfig())

    override fun close() {
        client.close()
    }

    fun url(url: String) = client.target(url)
    fun qwant(params: String) =
        url("https://api.qwant.com/api/search/images?offset=0&t=images&uiv=1&$params")
}