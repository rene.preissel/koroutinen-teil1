package de.e2.koroutine1

import com.jayway.jsonpath.JsonPath
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.response.HttpResponse
import io.ktor.client.response.readBytes
import io.ktor.http.HttpStatusCode
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.IOException
import javax.imageio.ImageIO

val ktorClient = HttpClient()
const val QWANT_URL_PREFIX = "https://api.qwant.com/api/search/images?offset=0&t=images&uiv=1"


suspend fun createCollage(query: String, count: Int): BufferedImage {
    val urls = requestImageUrls(query, count)
    val images = urls.map { requestImageData(it) }
    return combineImages(images)
}

suspend fun createCollageAsyncAwait(query: String, count: Int): BufferedImage {
    val urls = requestImageUrls(query, count)
    return coroutineScope {
        val deferredImages: List<Deferred<BufferedImage>> = urls.map {
            async { requestImageData(it) }
        }

        val images: List<BufferedImage> = deferredImages.awaitAll()
        combineImages(images)
    }
}

suspend fun loadOneImage(query: String): BufferedImage {
    val url = requestImageUrl(query)
    val image = requestImageData(url)
    return image
}

suspend fun requestImageUrl(query: String): String {
    val urls = requestImageUrls(query,20)
    val url = urls.shuffled().firstOrNull() ?: throw IllegalStateException("No image found")
    return url
}

suspend fun requestImageUrls(query: String, count: Int = 20): List<String> {
    val json = ktorClient.get<String>("${QWANT_URL_PREFIX}&q=$query&count=$count")
    val urls = JsonPath.read<List<String>>(json, "$..thumbnail").map { "https:$it" }
    return urls
}

suspend fun requestImageData(imageUrl: String): BufferedImage {
    val httpResponse = ktorClient.get<HttpResponse>(imageUrl)
    if (httpResponse.status == HttpStatusCode.OK) {
        val bytes = httpResponse.readBytes()
        val image = ImageIO.read(ByteArrayInputStream(bytes))
        return image
    }
    throw IOException("Wrong status code ${httpResponse.status}")
}

fun combineImages(imageList: Collection<BufferedImage>): BufferedImage {
    if (imageList.isEmpty()) {
        return BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR)
    }

    val yDim = Math.sqrt(imageList.size.toDouble()).toInt()
    val xDim = (imageList.size + yDim - 1) / yDim

    val maxDim = imageList.asSequence().map { Pair(it.width, it.height) }.fold(Pair(0, 0)) { a, b ->
        Pair(maxOf(a.first, b.first), maxOf(a.second, b.second))
    }

    val newImage = BufferedImage(maxDim.first * xDim, maxDim.second * yDim, BufferedImage.TYPE_3BYTE_BGR)
    val graphics = newImage.graphics
    graphics.color = Color.WHITE
    graphics.fillRect(0, 0, newImage.width, newImage.height)

    imageList.forEachIndexed { index, subImage ->
        val x = index % xDim
        val y = index / xDim
        val posX = maxDim.first * x + (maxDim.first - subImage.width) / 2
        val posY = maxDim.second * y + (maxDim.second - subImage.height) / 2
        graphics.drawImage(subImage, posX, posY, null)
    }
    return newImage
}
