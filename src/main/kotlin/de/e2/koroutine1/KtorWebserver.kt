package de.e2.koroutine1

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.response.respondOutputStream
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import javax.imageio.ImageIO

fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        routing {
            get("/collage/{query}") {
                val query = call.parameters["query"] ?: throw IllegalStateException("Query nicht angegeben")
                val count = call.request.queryParameters["count"]?.toInt() ?: 20
                val image = createCollageAsyncAwait(query, count)

                call.respondOutputStream(ContentType.Image.PNG) {
                    ImageIO.write(image, "png", this)
                }
            }
        }
    }
    server.start(wait = true)
}