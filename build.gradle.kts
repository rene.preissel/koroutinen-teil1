import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension

val kotlinCoroutineVersion by extra("1.1.0")
val ktorVersion: String by extra { "1.1.2" }


plugins {
    val kotlinVersion = "1.3.21"

    id("org.jetbrains.kotlin.jvm") version kotlinVersion
}

group = "de.e2"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
    maven("https://dl.bintray.com/kotlin/ktor")
    maven("https://kotlin.bintray.com/kotlinx")
}

configurations.forEach {
    it.exclude(group = "junit", module = "junit")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinCoroutineVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:$kotlinCoroutineVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:$kotlinCoroutineVersion")

    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")
    implementation("io.ktor:ktor-gson:$ktorVersion")
    implementation("com.jayway.jsonpath:json-path:2.3.0")

    implementation("io.reactivex.rxjava2:rxjava:2.2.6")
    runtimeOnly("org.apache.logging.log4j:log4j-slf4j-impl:2.11.1")

    testImplementation("org.glassfish.jersey.core:jersey-client:2.26")
    testImplementation("org.glassfish.jersey.inject:jersey-hk2:2.26")
    testImplementation("org.glassfish.jersey.media:jersey-media-json-jackson:2.26")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.3.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.3.1")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict -Xuse-experimental=kotlin.Experimental")
}